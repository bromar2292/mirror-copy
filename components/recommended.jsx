import React from "react";

import adverts from "../data/advert";


const Recommended = () => {
  return (
    <div className="recommended-container">
    <h4 className= 'recommended-title'>RECOMMENDED</h4>
      {adverts.map(selection => (
        <div className="recommendations">
          <img  className="recommended-images" src={selection.image} />
          <p className='recommended-text'> <b>{selection.description}</b></p>
        </div>
      ))}
    </div>
  );
};

export default Recommended;
