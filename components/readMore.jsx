import React from "react";

const ReadMore = () => {
  return (
    <>
      <div className="read-container">
        <div className="read-btn">
          <span className="M">M</span>
          <span className="read-more">READ MORE</span>
        </div>
        <p className="read-text">
          <b className="arrow2">></b>Thousands try their hand at drawing hack -
          and results are hilarious{" "}
        </p>
      </div>
      <div className="text">
        <p>
          The imitation on Reddit saw the figure's face changed to that of a
          dog.
        </p>
        <p>
          It was captioned: "Newsflash 'The Scream' has always been a floppy
          eared spaniel."
        </p>
        <p>
          Many people commented to say that they wouldn't be able to "unsee" the
          dog now and had never noticed before how similar to a spaniel the
          figure looked.
        </p>
        <p>One person said: "I won't be able to unsee that."</p>
        <p>Another wrote: "Can't unsee."</p>
        <p>
          A third added that from now on they'd always see a "dang spaniel" when
          they looked at the piece of artwork.
        </p>
      </div>
    </>
  );
};

export default ReadMore;
