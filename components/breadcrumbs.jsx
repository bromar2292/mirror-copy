import React from 'react';

const Breadcrumbs = () => {
    return (
        <div className= "second-nav">
        <span className= 'breadcrumbs-mobile'>NEWS</span>
        <div className= 'desktop'>
<span className='logo'><b>M</b></span>
<span className='arrow'><b>></b></span>
<span className='font'><b> News</b></span>
<span className='arrow'><b>></b></span>
<span className='font'><b>Weird News</b></span>
<span className='arrow'><b>></b></span>
<span className='font'><b>Art</b></span>
</div>
</div>



    )
}

export default Breadcrumbs;