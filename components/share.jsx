import React from "react";

const Share = () => {
  return (
    <div className="share-container">
    <div className='share-social-icons1'>
      <span className='share-color'>SHARE</span>
      <img className="social-icons" src="../static/FB-icon.png" />
      <img className="social-icons" src="../static/Twitter.png" />
      <img className="social-icons" src="../static/linkedin.png" />
      <img className="social-icons" src="../static/flipboard.png" />
      <img className="social-icons" src="../static/emaillogo.jpg" />
      </div>
    <div className='share-social-icons'>
      <img className="social-icons2"  src="../static/chatlogo.png" />
      <span className="comments" >COMMENTS</span>
      </div>
   <div className='share-social-icons'>
     <div className='audiance-writer'>
      <p className= 'audiance-writer-p'>
        By{" "}
        <b className='link-color'>
          <u>Courtney Pochin</u>{" "}
        </b>
        Audience Writer
       <b className='date'> 12:34, 25 OCT 2019 | <b>updated</b> 13:37, 25 OCT 2019</b>
      </p>
      </div>
      </div>
      <span className="logo-title2"> <b>NEWS</b></span>
    </div>
    
  );
};

export default Share;
