import React from "react";
import "../scss/_index.scss";
const Footer = () => {
  return (
    <div className="footer-container">
      <div className="footer-nav">
      <ul className='footer-nav-column'>
          <li>news</li>
          <li>sport</li>
          <li>film</li>
          <li>tech</li>
          <li>Tv</li>
          <li className='footer-reduce'>film</li>
          
          
       
          </ul>
             <ul className='footer-nav-column'>
          <li>money</li>
          <li>travel</li>
<li>mums</li>
          
          
          <li>quizzes</li>
          <li>football</li>
          <li className='footer-reduce'>politics</li>
       
       </ul>
             <ul className='footer-nav-column2'>
          <li>money</li>
          <li>travel</li>
<li>mums</li>
          <li>competitions</li>
          <li>motoring</li>
          <li className='footer-reduce'>competitions</li>
        
       
       </ul>
      </div>
      <div className="follow-us">
        <h5>FOLLOW US</h5>
        <div className="follow-us-icons">
          <img className="social-footer2" src="../static/fbcircle.png" />
          <img className="social-footer" src="../static/twitterCircle.png" />
          <img className="social-footer" src="../static/pintrestcircle.png" />
          <img className="social-footer" src="../static/instergramcircle.jpg" />
        </div>
        <div className='terms'>
          <p>Privacy policy</p>
          <p>Terms and Conditions</p>
          <p>Cookie policy</p>
        </div>
      </div>
    </div>
  );
};

export default Footer;
