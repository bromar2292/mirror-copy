import React from "react";

const Header = () => {
  return (
    <>
    <div className="header">
    <img className='mobile-menu ' src= '../static/menu2.png'/>
      <img className = 'header-logo' src="../static/logofinal.png" />

      <ul className="secondary">
        <li>news</li>
        <li>sport</li>
        <li>politics</li>
        <li>football</li>
        <li className= 'menu-reduce3'>celebs</li>
        <li className= 'menu-reduce3' >Tv</li>
        <li className= 'menu-reduce3'>film</li>
        <li className= 'menu-reduce2'>royals</li>
      
        <li className= 'menu-reduce2'>tech</li>
        <li className= 'menu-reduce2'>money</li>
        <li className= 'menu-reduce'>travel</li>
        <li className= 'menu-reduce'>fashion</li>
        
        <li className= 'menu-reduce'>quizzes</li>
        <li>more</li>
        </ul>
        <div className= 'social-container'>
        <img className="social-header2" src="../static/fbcircle.png" />
        <img className="social-header" src="../static/twitterCircle.png" />
        <img className="social-header" src="../static/pintrestcircle.png" />
        <img className="social-header" src="../static/instergramcircle.jpg" />
        <img className="magnifying-glass" src="../static/magnifyinglass.png" />
        <img className="account-icon" src="../static/account.png" />
        </div>
      
      
    </div>
    <ul className='second-menu'>
    <li>news</li>
        <li>sport</li>
        <li>politics</li>
        <li>football</li>
        <li>celebs</li>
        <li>tv</li>
      </ul>
      </>
  );
};

export default Header;
