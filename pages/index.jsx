import "../scss/_index.scss";
import Header from "../components/header";
import Footer from "../components/footer";
import Recommended from "../components/recommended";
import Subscribe from "../components/subscribe";
import Breadcrumbs from "../components/breadcrumbs";
import Share from "../components/share";
import Readmore from "../components/readMore";
const Index = () => (
  <>
    <Header />
    <div className="background">
      <div className="container">
        <div className="title">
          <div className="greyspace"></div>
          <Breadcrumbs />
          <h1>
            {" "}
            People spot something amusing about the scream painting- and its
            hard to unsee
          </h1>
          <p className="subtitle">
            Someone has shared an amusing take on edvard muchs 1893 painting and
            now people say they can't unsee it
          </p>
          <Share />
        </div>
        <div className="media">
          <div className="meida-left">
          
            <img className="image" src="../static/code.jpg" />
            <figcaption className='fig-caption'> The Scream is an iconic painting</figcaption>
            
            <Subscribe />
            <p className="text">
              It's one of the most iconic <b className='red-highlight'>paintings </b> out there and is instantly
              recognisable... or so you would think.
            </p>

            <p className="text">
              We're talking of course about Edvard Munch's composition, The
              Scream, or Der Schrei der Natur, which features a figure, standing
              in front of a striking orange background, holding his head in his
              hands, screaming.
            </p>

            <p className="text">
              However it seems not everyone knows what the original really looks
              like.
            </p>

            <p className="text">
              A  <b className='red-highlight'>Reddit</b> user shared a reimagined version of the artwork on the
              site and hilariously some people didn't even realise it wasn't the
              real painting - can you spot the difference?
            </p>

            <img className="fakescream" src="../static/fakescream.jpg" />
            <Readmore />
          </div>

          <div className="recommended-border">
            <Recommended />
          </div>
        </div>
      </div>
    </div>
    <Footer />
  </>
);

export default Index;
